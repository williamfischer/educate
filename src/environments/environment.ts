// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCKLm04yucpFxrZeT18MQjJvLuytltY9oU",
    authDomain: "ebounce-d389b.firebaseapp.com",
    databaseURL: "https://ebounce-d389b.firebaseio.com",
    projectId: "ebounce-d389b",
    storageBucket: "ebounce-d389b.appspot.com",
    messagingSenderId: "258154749088"
 }
};
