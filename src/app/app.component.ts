import { Component } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  itemRef: AngularFireObject<any>;
  itemRef2: AngularFireObject<any>;
  lessonCreate: AngularFireObject<any>;
  lessonCreate2: AngularFireObject<any>;


  itemcount: any;
  lessonCount: any;
  corelessons: any;
  videoRoomID: any;
  corelessons2: any[];
  roomNumber: number;
  totalcurrencyval: number;
  donateamt: string = '';
  email: string = '';

  subject: string = '';
  lesson: string = '';
  time: string = '';

  isWatchMode: boolean;
  isTeachMode: boolean;
  videoMode: boolean;
  thequickbrwnfx: boolean;

  subJect: string = '';
  lesSon: string = '';
  accesscode: string = '';
  currnetime: any;

  itemlist: Observable<any[]>;
  lessonCollect: Observable<any[]>;

  constructor(afDb: AngularFireDatabase, public sanitizer: DomSanitizer) {
    this.isWatchMode = false;
    this.isTeachMode = false;
    this.videoMode = false;

    this.corelessons2 = [];

    this.itemRef = afDb.object('supporters');
    this.itemlist = afDb.list('supporters').valueChanges();

    this.lessonCreate = afDb.object('lessons');

    this.totalcurrencyval = 0;

    this.itemlist.subscribe(snapshots=>{
        snapshots.forEach(snapshot => {
          this.totalcurrencyval = this.totalcurrencyval + snapshot.amount;
        });
    })

    this.itemRef.snapshotChanges().subscribe(action => {
      if(!action.payload.val().length){
        this.itemcount = 0
      }else{
        this.itemcount = action.payload.val().length
        this.itemRef2 = afDb.object('supporters/' + this.itemcount);
      }
    });

    this.lessonCreate.snapshotChanges().subscribe(action => {

      action.payload.val().forEach(snapshot => {
        console.log(this.corelessons2);

        this.corelessons2.push({
          roomnum: snapshot.roomnum,
          subject: snapshot.subject,
          lesson: snapshot.lesson,
          time: snapshot.time
        });
      });

      if(!action.payload.val().length){
        this.lessonCount = 0
      }else{
        this.lessonCount = action.payload.val().length
        this.lessonCreate2 = afDb.object('lessons/' + this.lessonCount);
      }
    });

  }

  watchMode(){
    // this.isWatchMode = true;
    // this.isTeachMode = false;

    if(this.accesscode == 'edu123'){
      this.isWatchMode = true;
      this.isTeachMode = false;
    }else{
      alert("Promo Code Invalid")
    }
  }

  teachMode(){
    // this.isTeachMode = true;
    // this.isWatchMode = false;

    if(this.accesscode == 'edu123'){
      this.isTeachMode = true;
      this.isWatchMode = false;
    }else{
      alert("Promo Code Invalid")
    }
  }

  backtofalse(){
    this.isTeachMode = false;
    this.isWatchMode = false;
    this.videoMode = false;
  }

  goVideo(corelesson){
    this.videoMode = true;
    this.roomNumber = corelesson.roomnum;
    this.subJect = corelesson.subject;
    this.lesSon = corelesson.lesson;
    this.currnetime =  Date.now();

    this.videoRoomID = this.sanitizer.bypassSecurityTrustResourceUrl('https://tokbox.com/embed/embed/ot-embed.js?embedId=12a84d87-8c40-416d-ba4e-b99962939cc9&room=' + corelesson.roomnum + '&iframe=true');

    this.thequickbrwnfx = true;
  }

  lessoncreation(){
    this.lessonCreate2.set({ roomnum: this.lessonCount+1, subject: this.subject, lesson: this.lesson, time: this.time });

    this.subject = '';
    this.lesson = '';
    this.time = '';
    alert("Session Created!")
  }

  support(){
    this.itemRef2.set({ count: this.itemcount+1, amount: this.donateamt, email: this.email });

    this.donateamt = '';
    this.email = '';
    alert("Donation Submitted!")
  }
}
